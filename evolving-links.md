# Engineering links

This is really just a mess of links grabbed from my bookmarks. It's all stuff I've found very useful or very interesting.

## Talks to watch
- https://github.com/AllThingsSmitty/must-watch-javascript
- https://github.com/AllThingsSmitty/must-watch-css
- https://github.com/bolshchikov/js-must-watch
- https://github.com/hellerve/programming-talks
- https://hacks.mozilla.org/2014/12/you-cant-go-wrong-watching-javascript-talks/
- https://medium.com/statuscode/great-programming-talks-to-watch-on-your-lunch-break-8b13e139929e
- https://vimeo.com/180566024

## Code study
- https://www.freecodecamp.com/challenges/reverse-a-string
- https://www.udacity.com/course/full-stack-foundations--ud088?utm_medium=referral&utm_campaign=api
- https://bento.io/
- https://hackr.io/
- https://thenewboston.com/
- https://frontendmasters.com/courses/es6-right-parts/
- https://frontendmasters.com/courses/javascript-foundations/
- https://frontendmasters.com/courses/data-structures-algorithms/
- https://frontendmasters.com/courses/advanced-javascript/
- https://frontendmasters.com/courses/testing-javascript/
- https://frontendmasters.com/courses/chrome-dev-tools/
- https://frontendmasters.com/courses/sass/
- http://xahlee.info/comp/comp_lang_tutorials_index.html
- https://confreaks.tv/events?page=2
- http://tutorials.codebar.io/

## Conferences
- http://www.catskillsconf.com/
- http://2017.empireconf.org/#schedule
- http://offlinefirst.org/camp/
- https://licensezero.com/
- http://www.wtfpl.net/

## Evergreen Resources
- https://repl.it/languages/javascript
- https://plnkr.co/edit/oL4hs65hE7ncGuf471Zm?p=preview
- https://learnxinyminutes.com/
- http://ricostacruz.com/cheatsheets/

## Languages

### elm
- https://egghead.io/courses/start-using-elm-to-build-web-applications
- https://thoughtbot.com/upcase/videos/elm-a-front-end-language-with-style
- https://www.elm-tutorial.org/en/01-foundations/01-hello.html
- https://guide.elm-lang.org/

### clojure

#### clojurescript
- https://shadow-cljs.github.io/docs/UsersGuide.html
- http://www.spacjer.com/blog/2014/09/12/clojurescript-javascript-interop/
- https://kanaka.github.io/clojurescript/web/synonym.html
- http://escherize.com/cljsfiddle/
- https://github.com/cognitect/transit-cljs/wiki/Getting-Started
- https://github.com/JulianBirch/cljs-ajax

#### reagant
- http://www.mattgreer.org/articles/reagent-rocks/
- https://reagent-project.github.io/
- https://github.com/reagent-project/reagent
- https://github.com/reagent-project/reagent-cookbook
- https://github.com/bbatsov/clojure-style-guide
- http://www.4clojure.com/
- https://clojure.org/api/cheatsheet
- http://www.fullstackclojure.com/
- https://medium.com/@roman01la/clojurescript-a-frontend-language-designed-for-efficient-state-management-52f145c2fee3#.41dz2ujuz
- https://practicalli.github.io/clojure-webapps/
- https://github.com/piranha/clojurescript-awesome
- http://stackoverflow.com/questions/37926881/how-can-i-communicate-with-the-backend-using-clojurescript-and-figwheel
- https://github.com/bvandgrift/crisco-compojure
- https://aphyr.com/posts/301-clojure-from-the-ground-up-welcome
- https://adambard.com/blog/a-simple-clojurescript-app/
- http://www.agilityfeat.com/blog/2015/03/clojure-walking-skeleton
- https://learnxinyminutes.com/docs/compojure/
- http://www.- http-kit.org/
- https://etherpad.net/p/Clojure_peeps_on_the_fediverse
- http://tonsky.me/blog/readable-clojure/
- https://lambdaisland.com/blog/25-05-2017-simple-and-happy-is-clojure-dying-and-what-has-ruby-got-to-do-with-it
- https://lambdaisland.com/p/about
- https://aphyr.com/posts/301-clojure-from-the-ground-up-welcome
- http://eli.thegreenplace.net/2017/reducers-transducers-and-coreasync-in-clojure/
- http://quil.info/examples
- http://www.daiyi.co/blog/2017/07/19/a-smol-comic-about-clojure/
- https://github.com/gigasquid/wonderland-clojure-katas
- http://eli.thegreenplace.net/2017/clojure-the-perfect-language-to-expand-your-brain/
- https://guide.freecodecamp.org/clojure
- https://www.anthony-galea.com/blog/post/hello-parking-garage-meet-clojure.spec/
- https://clojuriststogether.org/news/introducing-clojurists-together/
- https://www.conj.io/
- https://lambdaisland.com/blog/29-12-2017-the-bare-minimum-clojure-mayonnaise
- https://orestis.gr/25-days-of-clojure/
- https://clojureverse.org/
- https://guide.freecodecamp.org/clojure

### python
- https://www.fullstackpython.com/
- http://blog.underdog.io/post/152949056092/python-for-javascript-developers
- http://hg.toolness.com/python-for-js-programmers/raw-file/tip/PythonForJsProgrammers.html
- https://docs.python.org/3/tutorial/
- https://developers.google.com/edu/python/
- https://www.codecademy.com/courses/python-beginner-BxUFN/2/1?curriculum_id=4f89dab3d788890003000096
- https://flaskbook.com/#
- https://learnpythonthehardway.org/
- http://www.diveintopython.net/
- http://docs.python-guide.org/en/latest/
- https://automatetheboringstuff.com/
- https://www.fullstackpython.com/

#### django
- http://djangobook.com/
- https://www.djangoproject.com/start/
- http://wiki.c2.com/?LispProgramsForBeginners

### javaccript

#### es6
##### destructuring
- https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Destructuring_assignment
- https://ponyfoo.com/articles/es6-destructuring-in-depth
- http://wesbos.com/destructuring-objects/
- http://exploringjs.com/es6/ch_destructuring.html#sec_more-obj-destructuring
- https://angulartoreact.com/learning-center/modern-javascript/es6-destructuring/

#### general
- https://es6cheatsheet.com/
- http://es6-features.org/#Constants
- http://kangax.github.io/compat-table/es6/
- https://lebab.io/try-it
- http://es6-features.org/#ExpressionBodies
- https://ponyfoo.com/articles/es6-classes-in-depth
- http://www.codecademy.com/tracks/javascript
- http://javascriptissexy.com/how-to-learn-javascript-properly/
- http://www.reddit.com/r/learnjavascript/comments/1ceefw/learn_javascript_properly_omnibus_post/
- https://www.codeschool.com/courses/javascript-road-trip-part-1
- http://www.learnstreet.com/lessons/study/javascript
- http://www.lynda.com/course20/Developer-Programming-Foundations-tutorials/Making-most-course/83603/90427-4.html
- https://codeburst.io/javascript-map-vs-foreach-f38111822c0f
- https://chalarangelo.github.io/30-seconds-of-code/
- https://es6.io/
- https://docs.google.com/document/d/1bMSK0K-Sy6f3oGmdpCdHPORpqMKgfj6-uwNrPmxR1Qc/edit#heading=h.2letjmyb2h6c
- https://rmurphey.com/blog/2016/02/13/exercises-for-js-beginners
- http://www.asmarterwaytolearn.com/js/index-of-exercises.html
- https://developer.mozilla.org/en-US/docs/Web/JavaScript
- https://javascript30.com/
- https://medium.freecodecamp.org/the-definitive-javascript-handbook-for-a-developer-interview-44ffc6aeb54e
- https://johnresig.com/apps/learn/
- https://www.rithmschool.com/courses/intermediate-javascript
- http://frontend.turing.io/lessons/module-3/es6-spread-and-rest-operators.html

### ruby

#### haml
- https://learnxinyminutes.com/docs/haml/

#### rails
- http://guides.rubyonrails.org/index.html
- https://blog.passcod.name/2017/aug/15/an-explanation-of-rails-permit
- https://www.railstutorial.org/book/static_pages

#### testing
- http://www.agiletrailblazers.com/blog/the-5-step-guide-for-selenium-cucumber-and-gherkin
- http://learnseleniumtesting.com/gherkin-tutorial/
- https://github.com/tourdedave/cucumber-crash-course
- https://www.youtube.com/watch?v=lC0jzd8sGIA
- http://www.betterspecs.org/
- https://learnrubythehardway.org/book/
- https://github.com/bbatsov/ruby-style-guide
- https://launchschool.com/books
- http://www.rubyist.net/%7Eslagell/ruby/getstarted.html
- https://leanpub.com/rwdtow/read
- http://www.techotopia.com/index.php/Ruby_Essentials
- https://learnxinyminutes.com/docs/ruby/
- http://www.caliban.org/ruby/rubyguide.shtml
- http://tutorials.jumpstartlab.com/projects/ruby_in_100_minutes.html
- http://jasonkim.ca/projects/just_enough_ruby_to_get_by/
- http://blog.deveo.com/how-to-create-a-kanban-inspired-to-do-list-application-in-ruby-and-make-version-control-part-of-your-workflow/
- http://daydreamsinruby.com/blog/
- https://medium.freecodecamp.org/learning-ruby-from-zero-to-hero-90ad4eecc82d
- http://poignant.guide/book/expansion-pak-1.html
- https://medium.com/@sihui/what-the-heck-are-code-blocks-procs-lambdas-and-closures-in-ruby-2b0737f08e95
- http://www.brianstorti.com/understanding-ruby-idiom-map-with-symbol/
- http://ruby.bastardsbook.com/


## Dev Ops & Testing

### npm
- https://github.com/dwyl/learn-hapi
- http://stylus-lang.com/

### git
- https://sethrobertson.github.io/GitFixUm/fixup.html
- http://think-like-a-git.net/
- http://ohshitgit.com/
- http://gitready.com/

### jasmine
- https://medium.com/@kentcdodds/where-to-put-code-in-mocha-jasmine-tests-24aade62fd7e#.lrs5jcd7s
- https://medium.com/javascript-scene/what-every-unit-test-needs-f6cd34d9836d

### bash
- https://www.youtube.com/watch?v=BFMyUgF6I8Y
- https://www.devopslibrary.com/

## Comp Sci

### algorithms
- https://www.khanacademy.org/computing/computer-science/algorithms?ref=resume_learning#asymptotic-notation
- http://discrete.gr/complexity/

### design patterns
- https://en.wikipedia.org/wiki/Mediator_pattern
- https://en.wikipedia.org/wiki/Singleton_pattern
- https://en.wikipedia.org/wiki/Publish%E2%80%93subscribe_pattern
- https://teamtreehouse.com/library/creational
- https://sourcemaking.com/design_patterns/mediator
- https://addyosmani.com/resources/essentialjsdesignpatterns/book/

### software development
- https://www.udacity.com/course/software-development-process--ud805
- http://www.ccs.neu.edu/home/matthias/HtDP2e/
- https://www.gliffy.com/uses/uml-software/

### general
- https://github.com/open-source-society/computer-science
- http://blog.agupieware.com/2014/06/online-learning-intensive-bachelors.html
- https://github.com/open-source-society/computer-science
- http://spin.atomicobject.com/2015/05/15/obtaining-thorough-cs-background-online/
- https://github.com/mvillaloboz/open-source-cs-degree
- http://btholt.github.io/four-semesters-of-cs/
- https://www.aomran.com/pure-functional-programming/
- http://groups.csail.mit.edu/mac/classes/6.001/abelson-sussman-lectures/
- https://dev.to/ben/what-computer-science-concepts-should-devs-without-a-cs-background-prioritize-learning
- https://mitpress.mit.edu/sicp/

## Frontend & Libraries

### css
- https://tutorialzine.com/2017/03/css-grid-vs-flexbox
- https://christianheilmann.com/2017/09/19/web-truths-css-is-not-real-programming/
- https://medium.freecodecamp.org/how-to-make-your-html-responsive-by-adding-a-single-line-of-css-2a62de81e431
- https://chantastic.org/css4-to-css3/
- https://tomhodgins.github.io/qss/
- http://cssreference.io/?utm_source=frontendfocus&utm_medium=email
- https://cssreference.io/
- https://learn.shayhowe.com/advanced-html-css/
- https://smacss.com/
- http://howtocenterincss.com/
- http://colorsupplyyy.com/app/

#### flexbox
- https://medium.freecodecamp.com/an-animated-guide-to-flexbox-d280cf6afc35#.usw5eg4ys
- https://hacks.mozilla.org/2018/01/new-flexbox-guides-on-mdn/?utm_source=dev-newsletter&utm_medium=email&utm_campaign=jan11-2018
- https://www.sketchingwithcss.com/samplechapter/cheatsheet.html?__s=pqrjss1c6jtwbv8jwtp5
- https://philipwalton.github.io/solved-by-flexbox/
- http://flexboxfroggy.com/
- http://www.flexboxdefense.com/
- http://flexbox.io/

#### grid
- https://cssgrid.io/
- https://www.smashingmagazine.com/2017/12/grid-inspector/
- https://medium.freecodecamp.org/learn-css-grid-in-5-minutes-f582e87b1228
- https://cssgrid.cc/
- https://css-tricks.com/things-ive-learned-css-grid-layout/
- https://gridbyexample.com/examples/
- http://lea.verou.me/css3patterns/#
- https://rachelandrew.co.uk/archives/2016/03/25/making-sense-of-the-new-css-layout/

### css frameworks
- http://frowcss.com/
- http://tachyons.io/
- http://usewing.ml/
- https://www.getpapercss.com/
- https://jenil.github.io/chota/
- https://svelte.technology/blog/the-zen-of-just-writing-css?utm_source=ponyfoo+weekly&utm_medium=email&utm_campaign=80
- http://jeffcarp.github.io/frontend-hyperpolyglot/
- https://equinusocio.github.io/material-theme/

## Tools & Architecture

### api
- https://www.programmableweb.com/category/all/apis
- http://www.restapitutorial.com/lessons/- httpmethods.html

### ide
- https://atom.io/themes/princess-syntax
- http://nightcoders.net/
- https://cursive-ide.com/

### emacs
- http://melpa.org/#/
- http://spacemacs.org/
- http://emacsredux.com/blog/2017/12/31/into-to-cider/
- https://cb.codes/what-editor-ide-to-use-for-clojure/
- https://medium.freecodecamp.org/regular-expressions-demystified-regex-isnt-as-hard-as-it-looks-617b55cf787
- http://blog.meredithunderell.com/gulp-sass-bourbon-neat-browsersync-boilerplate/
- https://www.hacksplaining.com/

## Frameworks

### reason
- https://reasonml.github.io/
- https://discordapp.com/channels/235176658175262720/235176658175262720
- https://jaredforsyth.com/posts/a-reason-react-tutorial/
- https://jamesfriend.com.au/a-first-reason-react-app-for-js-developers
- https://jamesfriend.com.au/routing-in-reason-react

### react
- https://github.com/kriasoft/react-starter-kit/tree/feature/apollo
- http://www.thegreatcodeadventure.com/building-a-simple-crud-app-with-react-redux-part-1/
- https://hashnode.com/post/getting-started-with-es6-and-react-by-building-a-minimal-todo-app-citaix6xe04og8y531g491a1o
- https://www.codecademy.com/courses/react-101/lessons/your-first-react-component/exercises/import-react?action=lesson_resume&link_content_target=interstitial_lesson
- http://reactkungfu.com/2015/10/the-difference-between-virtual-dom-and-dom/
- https://syntax.fm/
- https://github.com/airbnb/javascript/tree/master/react

### node
- http://www.nodebeginner.org/
- http://stackoverflow.com/questions/2353818/how-do-i-get-started-with-node-js
- http://javascriptissexy.com/learn-node-js-completely-and-with-confidence/
- http://nodeschool.io/#workshopper-list
- http://keystonejs.com/docs/getting-started/
- https://ngrok.com/

## Challenges
- http://exercism.io/how-it-works/newbie
- http://codekata.com/
- https://www.codewars.com/users/sign_in
- http://cyber-dojo.org/
- https://pragprog.com/book/bhwb/exercises-for-programmers

## General Dev
- https://bigmachine.io/products/the-imposters-handbook
- http://codeunion.io/#faq
